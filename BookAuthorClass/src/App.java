public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Nam", "nam@gmail.com", 'm');
        Author author2 = new Author("Lan", "lan@gmail.com", 'f');
        System.out.println("Author 1 la: " + author1);
        System.out.println("Author 2 la: " + author2);
        Book book1 = new Book("Harry Potter", author1, 12.5);
        Book book2 = new Book("Hunger Game", author2, 16.75, 4);
        System.out.println("Book 1 la: " + book1);
        System.out.println("Book 2 la: " + book2);
    }
}
