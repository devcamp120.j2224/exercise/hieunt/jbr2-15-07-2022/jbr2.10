public class Book {
    String name;
    Author author;
    double price;
    int qty = 0;
    // khởi tạo có 3 tham số name, author, price
    public Book(String name,
    Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }
    // khởi tạo có đầy đủ tham số
    public Book(String name,
    Author author, double price, int qty) {
        this(name, author, price);
        this.qty = qty;
    }
    // getter setter
    public String getName() {
        return name;
    }
    public Author getAuthor() {
        return author;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }
    @Override
    public String toString() {
        return "Book[name=" + name 
        + ", " + author
        + ", price=" + price
        + ", qty=" + qty + "]";
    }
}
